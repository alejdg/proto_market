/* main module entry point */
//var log = require('_/log');
var cfg = require('./lib/config');
var app = require('./lib/app');
var appRoutes = require('./lib/app/routes');
var pack = require('./package');
var basic = require('./lib/auth').basic;

app.connection(	{
		host: cfg.host,
		port: cfg.port
	});


swaggerOptions = {
    basePath: 'http://' + cfg.host + ':' + cfg.port,
    apiVersion: pack.version
};


app.register({
    register: require('hapi-swagger'),
    options: swaggerOptions
}, function (err) {
    if (err) {
        console.log(['error'], 'hapi-swagger load error: ' + err)
    }else{
        console.log(['start'], 'hapi-swagger interface loaded');
    }
});


app.register({
    register: require('hapi-auth-basic'),
}, function (err) {
    if (err) {
        console.log(['error'], 'hapi-auth-basic load error: ' + err)
    }else{
        console.log(['start'], 'hapi-auth-basic loaded');
    }
});

app.auth.strategy('simple', 'basic', { validateFunc: basic.validate });

app.route(appRoutes);


app.start(function () {
	console.log('Server running at: ' + app.info.uri);
});

