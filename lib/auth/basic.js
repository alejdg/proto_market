var users = require('../app/models/users')();
var Bcrypt = require('bcrypt-nodejs');
var md5 = require('MD5');


exports.validate = function (username, password, callback) {

	getUser(username, function (err, user) {

		if (err) {
			console.log(err);
			return callback(err);
		}

		Bcrypt.compare(password, user.password, function (err, isValid) {

				callback(err, isValid, { id: user.id, name: user.name });
			});
	});
};

var getUser = function (username, callback) {

	users.findByUserName(username, callback);
};
