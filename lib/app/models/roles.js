module.exports =   Roles;
var cfg = require('../../config');
var Bcrypt = require('Bcrypt-nodejs')

//Roles constructor

function Roles() {
  if (!(this.constructor === Roles)) {
    return new Roles();
  }

  //Requiring monk to connect to the mongodb

  var db = require('monk')(cfg.db_host + ':' + cfg.db_port + '/' + cfg.db_name);

  //Acessing the roles collection

  this.roles = db.get('roles');
};

Roles.prototype.findAll = function (next) {

  this.roles.find({}, next);
};


Roles.prototype.findById = function (id, next) {

  this.roles.findById( id, next);
};


Roles.prototype.findByUserName = function (name, next) {

  this.roles.findOne({ name: name }, next);
};


Roles.prototype.create = function (role, next) {

  hashUserPassword(role, insertBridge(callback));
};


Roles.prototype.update = function (role, next) {

  var query = { 
    $set: { 
      rolename: role.rolename,
      password: role.password
    }
  };

  this.roles.findAndModify(role._id, query, next);
};


Roles.prototype.upsert = function (role, next) {

  var self = this;

  console.log(typeof this.roles);
  hashUserPassword(role, upsertBridge(self, next));
};


Roles.prototype.updateByUserName = function updateByUserName(role, next) {

  var query = { 
    $set: { 
      rolename: role.rolename, 
      password: role.password 
    }
  };

  this.roles.findAndModify(role.rolename, query, next);
};


Roles.prototype.removeById = function (id, next) {

  this.roles.remove({ _id: id }, next);
};


var hashUserPassword = function (role, callback) {

  Bcrypt.hash(role.password, null, null, function(err, hash) {
    if (err) {
      console.log(['error'], err);
      return err;
    }

    role.password = hash;
    return callback(null, role);
  });
};


var insertBridge = function (callback) {

  return function (err, role) {
    if (err) {
      return callback(err);
    }

    this.roles.insert(role, callback);
  };
};


function upsertBridge (self, callback) {

  return function (err, role) {
    if (err) {
      return callback(err);
    }

    console.log(typeof self.roles);
    return self.roles.update({ 'rolename': role.rolename }, role, {upsert: true}, callback);
  };  
};

