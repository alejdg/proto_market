module.exports =	 Users;
var cfg = require('../../config');
var Bcrypt = require('Bcrypt-nodejs')

//Users constructor

function Users() {
	if (!(this.constructor === Users)) {
		return new Users();
	}

	//Requiring monk to connect to the mongodb

	var db = require('monk')(cfg.db_host + ':' + cfg.db_port + '/' + cfg.db_name);

	//Acessing the users collection

	this.users = db.get('users');
};

Users.prototype.findAll = function (next) {

	this.users.find({}, next);
};


Users.prototype.findById = function (id, next) {

	this.users.findById( id, next);
};


Users.prototype.findByUserName = function (username, next) {

	this.users.findOne({ username: username }, next);
};


Users.prototype.create = function (user, next) {

	hashUserPassword(user, insertBridge(callback));
};


Users.prototype.update = function (user, next) {

	var query = { 
		$set: { 
			username: user.username,
			password: user.password,
			userId: user.userId,
			name: user.name,
			phoneNumber: user.phoneNumber,
			iccId: user.iccId,
			imei: user.imei,
			role: user.role
		}
	};

	this.users.findAndModify(user._id, query, next);
};


Users.prototype.upsert = function (user, next) {

	var self = this;

	console.log(typeof this.users);
	hashUserPassword(user, upsertBridge(self, next));
};


Users.prototype.updateByUserName = function updateByUserName(user, next) {

	var query = { 
		$set: { 
			username: user.username, 
			password: user.password 
		}
	};

	this.users.findAndModify(user.username, query, next);
};


Users.prototype.removeById = function (id, next) {

	this.users.remove({ _id: id }, next);
};


var hashUserPassword = function (user, callback) {

	Bcrypt.hash(user.password, null, null, function(err, hash) {
		if (err) {
			console.log(['error'], err);
			return err;
		}

		user.password = hash;
		return callback(null, user);
	});
};


var insertBridge = function (callback) {

	return function (err, user) {
		if (err) {
			return callback(err);
		}

		this.users.insert(user, callback);
	};
};


function upsertBridge (self, callback) {

	return function (err, user) {
		if (err) {
			return callback(err);
		}

		console.log(typeof self.users);
		return self.users.update({ 'username': user.username }, user, {upsert: true}, callback);
	};	
};

