exports.reply = function (reply) {

    return function(error, data) {

        if (error) {
            return reply(error);
        }

        return reply(data);
    };
};

