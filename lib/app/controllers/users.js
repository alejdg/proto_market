var users = require('../models/users')();
var bridges = require('./bridges');


exports.findAll = function(request, reply) {

	users.findAll(bridges.reply(reply));
};


exports.findById = function(request, reply) {

	users.findById(id, bridges.reply(reply));
};

exports.findByUserName = function(request, reply) {
	
	users.findByUserName(username, bridges.reply(reply));
};

exports.create = function(request, reply) {
	
	users.create(request.payload, bridges.reply(reply));
};

exports.upsert = function(request, reply) {
	
	users.upsert(request.payload, bridges.reply(reply));
};


exports.update = function(request, reply) {
	
	users.update(request.payload.user, bridges.reply(reply));
};

exports.removeById = function(request, reply) {
	
	users.removeById(request.payload.id, bridges.reply(reply));
};	

