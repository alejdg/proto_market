var controllers = {}
controllers.users = require('./users');
controllers.usersLastLocation = require('./usersLastLocation');

module.exports = controllers;