var controllers = require('./controllers');
var Joi = require('joi');

module.exports =[
{
  method: 'GET',
  path:'/teste', 
  handler: function (request, reply) {
     reply('Api UP!');
  }
},

//users

{
  method: 'GET',
  path:'/users',
  config: {
    auth: 'simple',
    handler: controllers.users.findAll,
    notes: 'Returns all users',
    tags: ['api']
  } 
},
{
  method: 'GET',
  path:'/users/{username}',
  config: {
    handler: controllers.users.findByUserName,
    notes: 'Returns user by username',
    tags: ['api'],
    validate: {
      params: {
        username: Joi.string().required(),
      }
    }
  } 
},
{
  method: 'PUT',
  path:'/users',
  config: {
    handler: controllers.users.upsert,
    notes: 'Create or update an user',
    tags: ['api'],
    validate: {
      payload: {
        username:     Joi.string().required(),
        password:     Joi.string().required(),
        userId:       Joi.string().required(), // Código do funcionário na empresa
        name:         Joi.string().required(),
        phoneNumber:  Joi.string(11).required(), // Número da linha telefônica
        iccId:        Joi.string(11).required(), // ICC ID -> ID chip de dados
        imei:         Joi.string(11).required(), // IMEI -> ID Aparelho
        role:         Joi.string()  

        /*
          Código do funcionário no nosso sistema
          Código do funcionário na empresa
          Nome do Funcionário
          Número da linha telefônica
          ICC ID -> ID chip de dados
          IMEI -> ID Aparelho
        */
      }
    }
  } 
},
{
  method: 'DELETE',
  path:'/users', 
  config: {
    handler: controllers.users.removeById,
    notes: 'Deletes a user by the id passed in the request body',
    tags: ['api'],
    validate: {
      payload: {
        id: Joi.string().required()
      }
    }
  } 
},

//users/lastLocation

{
  method: 'GET',
  path:'/users/lastLocation', 
  config: {
    handler: controllers.usersLastLocation.findAll,
    notes: 'Returns all users last locations',
    tags: ['api'] 
  }
},
{
	method: 'GET',
  path:'/users/lastLocation/{username}', 
  config: {
    handler: controllers.usersLastLocation.findByUserName,
    notes: 'Returns the user last location by the id passed in the URL',
    tags: ['api'] ,
    validate: {
      params: {
        username: Joi.string().required(),
      }
    }
  } 
},
{
  method: 'GET',
  path:'/users/lastLocation/near/{username}', 
  config: {
    handler: controllers.usersLastLocation.findNearUserName,
    notes: 'Returns all users last location near the user passed in the URL',
    tags: ['api'],
    validate: {
      params: {
        username: Joi.string().required(),
      }
    }
  }
},
{
	method: 'PUT',
  path:'/users/lastLocation', 
  config: {
    handler: controllers.usersLastLocation.upsert,
    notes: 'Set the user last location',
    tags: ['api'],
    validate: {
      payload: {
        username: Joi.string().required(),
        loc: Joi.object().keys({
          long: Joi.number().min(-180).max(180).required(),
          lat: Joi.number().min(-90).max(90).required(),
        }).required()
      }
    }
  } 
}]
